#include <stdio.h>
#include <stdlib.h>

typedef struct Node Node;

struct Node{
    int val;
    Node *next;
    Node *prev;
};

Node *make_node(int val) {
    Node *new = malloc(sizeof(Node));
    new->prev = NULL;
    new->next = NULL;
    new->val = val;
    return new;
}

void destroy_list(Node *node) {
    if (node->prev == NULL) {
        while(node->next != NULL) {
            Node *temp = node;
            node = node->next;
            free(temp);
        }
    } else if (node->next == NULL) {
        while(node->prev != NULL) {
            Node *temp = node;
            node = node->prev;
            free(temp);
        }
    } else {
        printf("This function takes the first or last node of list");
    }
}

Node *append(Node *node, int val) {
    Node *next = make_node(val);
    node->next = next;
    return next;
}

void print_list(Node *start) {
    while(start != NULL) {
        printf( "%d\n", start->val );
        start = start->next;
    }
}

int main(void) {
    int input_len;
    puts("Length of list? ");
    scanf("%d", &input_len);

    int i = 0;
    Node *start = make_node(i++); 
    Node *end = start;
    for(; i < input_len; i++) {
        end = append( end, i );
    }

    print_list(start);

    destroy_list(end);
}