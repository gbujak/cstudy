#include <stdio.h>

int main(void) {
	int num;
	char *strs[] = {"jeden", "dwa", "trzy", "cztery", "pięć", "sześć", "siedem", "osiem", "dziewięć"};

	printf("Podaj wartość -> ");
	scanf(" %d", &num);

	for (int i = num - 1; i >= 0; --i)
		printf("%s\n", strs[i]);

	/*
	switch (num) {
		case 9:
			printf("dziewięć\n");
		case 8:
			printf("osiem\n");
		case 7:
			printf("siedem\n");
		case 6:
			printf("sześć\n");
		case 5:
			printf("pięć\n");
		case 4:
			printf("cztery\n");
		case 3:
			printf("trzy\n");
		case 2:
			printf("dwa\n");
		case 1:
			printf("jeden\n");
			break;
		default:
			printf("Podaj numer pomiędzy 1 a 9!\n");
	}
	*/
}
