#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define SIZE 32

enum state {DEAD,ALIVE};

unsigned char board[SIZE][SIZE];

unsigned int down (unsigned int y) {
    return (y + 1) % SIZE;
}

unsigned int up (unsigned int y) {
    return (y + (SIZE -1)) % SIZE;
}

unsigned int right (unsigned int x) {
    return down(x);
}

unsigned int left (unsigned int x) {
    return up(x);
}

unsigned char cound_alive_neighbours (
    unsigned char board[SIZE][SIZE],
    unsigned int i, unsigned int j
) {
    return board[i][down(j)]
        +  board[i][up(j)]
        +  board[left(i)][j]
        +  board[right(i)][j]
        +  board[right(i)][down(j)]
        +  board[right(i)][up(j)]
        +  board[left(i)][down(j)]
        +  board[left(i)][up(j)];
}

void get_next_step (unsigned char board[SIZE][SIZE]) {
    static unsigned char swap[SIZE][SIZE];
    unsigned int i,j;

    for (i=0;i<SIZE;i++)
        for (j=0;j<SIZE;j++) {
            unsigned char state = board[i][i];
            unsigned char alive_neighbours = cound_alive_neighbours(board, i, j);
            if (state == ALIVE && alive_neighbours < 2)
                swap[i][j] = DEAD;
            if (state == ALIVE && alive_neighbours > 3)
                swap[i][j] = DEAD;
            if (state == ALIVE && (alive_neighbours == 3 || alive_neighbours == 2))
                swap[i][j] = ALIVE;
            if (state == DEAD && alive_neighbours == 3)
                swap[i][j] = ALIVE;
        }
    memcpy(board, swap, SIZE*SIZE);
}

void seed_board(unsigned char board[SIZE][SIZE]){
    unsigned int i,j,k;
    srand(time(NULL));
    for(k = 0; k<8; k++) {
        i = rand()%SIZE;
        j = rand()%SIZE;
        board[i][j] = ALIVE;
        int choice = rand()%8;
        switch(choice) {
        case 0 :
            board[i][down(j)] = ALIVE;
        case 1 :
            board[i][up(j)] = ALIVE;
        case 2 :
            board[left(i)][j] = ALIVE;
        case 3 :
            board[right(i)][j] = ALIVE;
        case 4 :
            board[right(i)][down(j)] = ALIVE;
        case 5 :
            board[right(i)][up(j)] = ALIVE;
        case 6 :
            board[left(i)][down(j)] = ALIVE;
        case 7 :
            board[left(i)][up(j)] = ALIVE;
        }
    }
}

void create_blinker(unsigned char board[SIZE][SIZE]){
    board[SIZE/2-1][SIZE/2-1] = board[SIZE/2][SIZE/2-1]
    = board[SIZE/2+1][SIZE/2-1] = ALIVE;
}

void create_ten_in_row(unsigned char board[SIZE][SIZE]){
    memset((void *)&board[SIZE/2-1][SIZE/2-6],ALIVE,10);
}

void print_board(unsigned char board[SIZE][SIZE]) {
    unsigned int i,j;
    for(i=0; i<SIZE; i++) {
    printf("\n");
    for(j=0; j<SIZE; j++)
        printf("%2d",board[i][j]);
    }
    printf("\n");
}

int main(void) {



}