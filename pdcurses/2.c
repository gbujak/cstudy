#include <ncurses.h>

void boxwin(WINDOW *wins[], int index) {
    box(wins[index], 0, 0);
    wrefresh(wins[index]);
}

void unboxwin(WINDOW *wins[], int index) {
    werase(wins[index]);
    wrefresh(wins[index]);
}

int main(void) {
    initscr();
    noecho();
    refresh();

    WINDOW *wins[4] = {
        newwin(LINES/2,COLS/2,0,0),
        newwin(LINES/2,COLS/2,0,COLS/2),
        newwin(LINES/2,COLS/2,LINES/2,0),
        newwin(LINES/2,COLS/2,LINES/2,COLS/2)
    };

    char input;
    int removing = 0;
    int index = 0;

    while ((input = getchar()) != 27) {
        if (input == 9) {
            if (!removing) {
                boxwin(wins, index++);
            } else {
                unboxwin(wins, index++);
            }
            if(index > 3) {
                index = 0;
                removing ^= 1;
            }
        }
    }
    endwin();
}