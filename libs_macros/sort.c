#include"sort.h"

static void swap(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

void sort(array_type array, int reverse) {
    int i;
    if(!reverse){
        for(i=0;i<sizeof(array_type)/sizeof(int)-1;i++) {
            int minimum,j;
            minimum=i;
            for(j=i;j<sizeof(array_type)/sizeof(int);j++)
                if(array[j]<array[minimum])
                    minimum=j;
            if(minimum!=i)
                swap(&array[i],&array[minimum]);
        }
    } else {
        for(i=0;i<sizeof(array_type)/sizeof(int)-1;i++) {
            int maximum,j;
            maximum=i;
            for(j=i;j<sizeof(array_type)/sizeof(int);j++)
                if(array[j]>array[maximum])
                    maximum=j;
            if(maximum!=i)
                swap(&array[i],&array[maximum]);
        }
    }
}