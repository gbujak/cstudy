#include <assert.h>
#include "sort.h"

void assert_sorted(array_type, int);

int main(void) {
    int reverse = 0;

    array_type arr;
    fill_array(arr, 21, -10);

    sort(arr, reverse);

    // arr[5] = 100;

    assert_sorted(arr, reverse);
}

void assert_sorted(array_type arr, int reverse) {
    for (int i = 0; i < 10 - 1; i++) {
        if(!reverse){
            assert(arr[i] <= arr[i+1]);
        } else {
            assert(arr[i] >= arr[i+1]);
        }
    }
}