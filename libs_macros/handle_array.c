#include"sort.h"
#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void print_array(array_type array)
{
    int i;
    for(i=0;i<sizeof(array_type)/sizeof(int);i++)
        printf("%d ",array[i]);
    puts("");
}

void fill_array(array_type array, int range, int offset) {
    srandom(time(0));
    int i;
    for(i=0;i<sizeof(array_type)/sizeof(int); i++)
        array[i] = offset+random()%range;
}