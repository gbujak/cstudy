#ifndef SORT_H
#define SORT_H

typedef int array_type[10];

extern void sort(array_type, int);

extern void print_array(array_type);

extern void fill_array(array_type, int, int);

#endif