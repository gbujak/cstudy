#include <stdio.h>
#define BIEDA_PRINTF(ZMIENNA,FORMATS) do { \
    printf("Wyrazenie "); \
    printf(#ZMIENNA ); \
    printf(" ma wartosc "); \
    printf(FORMATS, (ZMIENNA)); \
    printf(" w linii numer %d w pliku %s \n", __LINE__, __FILE__); \
} while (0)

int main(void) {
    int tmp = 5;
    BIEDA_PRINTF(tmp, "%d");


}