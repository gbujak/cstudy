#include <stdio.h>
#include <string.h>

int has_abba(char[]);

int is_abba(char[]);

int main(void) {
	char input[100];

	do {
		printf("podaj ciąg liter a/b o długości 10 znaków: ");
		scanf("%s", input);
	} while (strlen(input) != 10);

	if (has_abba(input)) {
		puts("Znajduje sie abba!");
	} else {
		puts("nie ma abby.");
	}
}

int has_abba(char in[]) {
	for(int i = 0; i <= 6; i++) {
		if (in[i] == 'a' && in[i+1] == 'b' && in[i+2] == 'b' && in[i+3] =='a')
			return 1;
	}
	return 0;
}

int is_abba(char in[]) {
	char abba[5] = "abba";
	for(int i = 0; i <= 3; i++) {
		if (in[i] != abba[i]){
			return 0;
		}
	}
	return 1;
}
