#include <stdio.h>
#include <string.h>

int is_anagram(char str[], int pool[]) {
    for (int i; i < strlen(str); i++) {
        pool[ str[i] ]--;
    }
    for (int i; i < 256; i++) {
        if(pool[i] != 0) {
            return 0;
        }
    }
    return 1;
}

int main (void) {
    char str[100] = "";
    char str2[100] = "";
    scanf("%s", str);

    int pool[256] = {0};

    for (int i = 0; i < strlen(str); i++) {
        pool[ str[i] ]++;
    }
    scanf("%s", str2);

    printf("anagram? %d\n", is_anagram(str2, pool));
}