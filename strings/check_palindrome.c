#include <stdio.h>
#include <string.h>

void reverse_str(char str[], char reverse[]) {
    int len = strlen(str);
    for (int i = 0; i < len; i++) {
        reverse[len - i - 1] = str[i];
    }
}

int main(void) {
    char reverse[100] = "";
    char palindrome[100] = "";
    printf("word? --> ");
    
    scanf("%s", palindrome);

    reverse_str(palindrome, reverse);

    int is_palindrome = strcmp(palindrome, reverse);
    
    if(is_palindrome == 0) {
        is_palindrome = 1;
    } else {
        is_palindrome = 0;
    }
    
    printf("%s\n%s\nstring is palindrome ? %d\n", palindrome, reverse, is_palindrome);
}