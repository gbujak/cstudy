#include <ncurses.h>
#include <stdio.h>

int main(void) {
    char login[100];
    char passw[100];

    initscr();

    printw("Login:  ");
    scanw("%s", login);

    noecho();

    printw("Passwd:  ");
    scanw("%s", passw);

    endwin();

    printf("login: %s\npasswd: %s\n", login, passw);
}