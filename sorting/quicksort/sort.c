#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LEN 30

void printArray (int arr[]) {
    for(int i = 0; i < LEN; i++) {
        printf("%d, ", arr[i]);
    }
    printf("\n");
}

void swap (int *from, int *to) {
    int temp = *to;
    *to = *from;
    *from = temp;
}

int partition (int arr[], int low, int high) {
    int pivot = arr[high];
    int i = low - 1;
    for(int j = low; j < high; j++) {
        if (arr[j] <= pivot) {
            i++;
            swap(arr+i, arr+j);
        }
    }
    swap(arr + i + 1, arr + high);
    return i + 1;
}

void quicksort (int arr[], int low, int high) {
    if (low < high) {
        int pi = partition(arr, low, high);

        quicksort(arr, low, pi - 1);
        quicksort(arr, pi + 1, high);
    }
}

int main (void) {
    srand(time(NULL));
    int arr[LEN];
    for(int i = 0; i < LEN; i++){
        arr[i] = rand() % 100;
    }
    printArray(arr);

    quicksort(arr, 0, LEN - 1);

    printArray(arr);
}