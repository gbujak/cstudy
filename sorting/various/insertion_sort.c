#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int LEN = 30;

void insertion_sort (int arr[]);
void print_array (int arr[], int length);

int main(void) {

    srand(time(NULL));
    int arr[LEN];
    for (int i = 0; i < LEN; i++) {
        arr[i] = rand() % 11;
    }
    print_array(arr, LEN);

    insertion_sort(arr);
    print_array(arr, LEN);
    
}

void insertion_sort (int arr[]) {
    int i;
    for(i = 0; i < LEN; i++) {
        int key = arr[i];
        int j = i-1;
        while(j >= 0 && arr[j] > key) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = key;
    }
}

void print_array (int arr[], int length) {
    for(int i = 0; i < length; i++) {
        printf("%2d, ", arr[i]);
    }
    printf("\n");
}