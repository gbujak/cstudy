#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LEN 30
#define MAX_SIZE 8

void print_array (int arr[], int length) {
    for(int i = 0; i < length; i++) {
        printf("%d, ", arr[i]);
    }
    printf("\n");
}

void get_repetitions(int arr[], int repetitions[]) {
    for (int i = 0; i < LEN; i++) {
        repetitions[arr[i]]++;
    }
}

void sort_repetition(int arr[], int repetitions[]) {
    int index = 0;
    for (int i = 0; i <= MAX_SIZE; i++) {
        for (int j = 0; j < repetitions[i]; j++) {
            arr[index] = i;
            index++;
        }
    }
}

int main (void) {
    srand(time(NULL));
    
    int arr[LEN];
    for(int i = 0; i < LEN; i++){
        arr[i] = rand() % MAX_SIZE + 1;
    }

    int repetitions[MAX_SIZE + 1] = {0}; // tabela z polami dla wartości od 0 do 8 
    // = {0} żeby wszystkie pola tabeli miały wartość 0, a nie losową

    get_repetitions(arr, repetitions);

    print_array(arr, LEN);

    int sorted[LEN];

    sort_repetition(sorted, repetitions);

    print_array(sorted, LEN);
}