#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LEN 30

void print_array (int arr[], int length);
void swap (int *from, int *to);
void print_repetitions (int arr[]);
void get_repetitions (int arr[], int repetitions[]);



int main (void) {
    // 1
    srand(time(NULL));
    int arr[LEN];
    for(int i = 0; i < LEN; i++){
        arr[i] = rand() % 11;
    }
    print_array(arr, LEN);
    print_repetitions(arr);
    // 2, 3
    for(int i = 0; i < LEN; i++){
        arr[i] = rand() % 9;
    }
    int repetitions[8] = {0};
    get_repetitions(arr, repetitions);
    print_array(repetitions, 8);
};



void print_array (int arr[], int length) {
    for(int i = 0; i < length; i++) {
        printf("%d, ", arr[i]);
    }
    printf("\n");
}

void swap (int *from, int *to) {
    int temp = *to;
    *to = *from;
    *from = temp;
}

void print_repetitions (int arr[]) {
    int repetitions[LEN] = {0};

    for (int i = 0; i < LEN; i++) {
        repetitions[arr[i]]++;
    }

    printf("repetitions : ");
    print_array(repetitions, LEN);
}

void get_repetitions(int arr[], int repetitions[]) {
    for (int i = 0; i < LEN; i++) {
        repetitions[arr[i]]++;
    }
}