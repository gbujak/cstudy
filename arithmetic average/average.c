#include <stdio.h>
#include <stdlib.h>

int main (void) {
    int *all = malloc(1000*sizeof(int));
    int index = 0;
    int input = 0;
    int sum = 0;

    do {
        scanf("%d", &input);
        all[index] = input;
        index++;
    } while (input);

    for(int i = 0; i <= index; i++) {
        sum += all[i];
    }

    printf("suma = %d\n", sum);

    float average = ((float) sum) / ((float) index -1);

    printf("The arithmetic average equals %f\n", average);

    free(all);
    return 0;
}
