#include <stdio.h>
#include <string.h>

#define FILE1 "sample.txt"
#define FILE2 "sample2.txt"
#define LEN 200

void readline(FILE *f, char text[]) {
    int i = 0;
    char ch = fgetc(f);
    while (!feof(f) && ch != '\n') {
        text[i] = ch;
        i++;
        ch = fgetc(f);
    }
    text[i] = '\0';
}

int eqline(char a[], char b[]) {
    return !strcmp(a, b);
}

int main(void) {
    FILE *f1 = fopen(FILE1, "r");
    FILE *f2 = fopen(FILE2, "r");

    char linef1[LEN], linef2[LEN];

    int linecount = 1;

    while (!feof(f1) && !feof(f2)) {
        readline(f1, linef1);
        readline(f2, linef2);
        if (!eqline(linef1, linef2)) {
            printf("roznica linijka : %d\n", linecount);
            puts(linef1);
            puts(linef2);
        }
        linecount++;
    }

}
