#include <stdio.h>

void pass(FILE *f) {
    while (!feof(f)) {
        putc( fgetc(f), stdout );
    }
}

int main (int argc, char **argv) {
    if (argc != 2) {
        puts("podaj nazwe jednego pliku");
        return 1;
    }

    FILE *f = fopen(argv[1], "r");
    if (f == NULL) {
        puts("blad otwierania pliku");
        return 1;
    }

    pass(f);

    if (fclose(f)) {
        puts("Blad zamkniecia pliku");
        return 1;
    }

    return 0;
}