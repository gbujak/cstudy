#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FNAME "matrix.csv"
#define SIZE 4

void gen_matrix(int m[SIZE][SIZE]) {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            m[i][j] = (rand() % 21) - 10;
        }
    }
}

void print_matrix(int m[SIZE][SIZE]) {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            printf("%3d,", m[i][j]);
        }
        printf("\n");
    }
}

void store_matrix(int m[SIZE][SIZE], FILE *f) {
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            fprintf(f, "%d", m[i][j]);
            if (j==(SIZE-1)) fputc('\n', f);
            else fputc(',', f);
        }
    }
}

void read_matrix(int m[SIZE][SIZE], FILE *f) {
    for(int i = 0; i < SIZE; i++) {
        for(int j = 0; j < SIZE; j++) {
            fscanf(f, "%d", &m[i][j]);
            fgetc(f); // pozbywa się przecinka albo '\n'
        }
    }
}

int main(void) {
    int m  [SIZE][SIZE];
    int m2 [SIZE][SIZE];
    FILE *f;

    srand(time(NULL));

    gen_matrix(m);
    print_matrix(m);

    if ((f = fopen(FNAME, "w+")) == NULL) {
        puts("blad otwierania");
        return 1;
    }

    store_matrix(m, f);

    if(fseek(f, 0, SEEK_SET) == -1){
        puts("blad przesuwania wskaznika pliku");
        return 1;
    }

    read_matrix(m2, f);

    puts("=================");

    print_matrix(m2);
}