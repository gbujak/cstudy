#include <stdio.h>

#define SOURCE "2.c"

void passmultline(FILE *f) {
    int starcount = 0;
    char current;
    while (!feof(f)) {
        current = fgetc(f);
        switch (current) {
            case '*':
                starcount++;
                break;
            case '/':
                if (starcount == 1)
                    return;
            default:
                starcount = 0;
        }
        putc(current, stdout);
    }
}

void passline(FILE *f) {
    char current;
    while (!feof(f) && current != '\n') {
        current = fgetc(f);
        putc(current, stdout);
    }
    putc('\n', stdout);
}

void comments(FILE *f) {
    int slashcount = 0;
    char current;
    while (!feof(f)) {
        current = fgetc(f);
        switch (current) {
            case '/':
                if (slashcount == 1) {
                    slashcount = 0;
                    passline(f);
                    break;
                }
                slashcount++;
                break;
            case '*':
                if (slashcount == 1) {
                    slashcount = 0;
                    passmultline(f);
                    break;
                }
            default:
                slashcount = 0;
        }
    }
} 
// test
/*
wielolinijkowy test
*/
int main(void) {
    FILE *f;
    if ((f = fopen(SOURCE, "r")) == NULL) {
        puts("blad otwierania");
        return 1;
    }

    comments(f);

    if (fclose(f)) {
        puts("blad zamykania");
    }
}