#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define TABLE_SIZE 10

void print_table(int *t) {
    for (int i = 0; i < TABLE_SIZE - 1; i++)
        printf("%2d, ", t[i]);
    printf("%2d\n", t[TABLE_SIZE - 1]);
}

int main (void) {
    int t[TABLE_SIZE];
    int flip[TABLE_SIZE];

    srand(time(0));
    for (int i = 0; i < TABLE_SIZE; i++) {
        t[i] = rand() % 20;
    }

    memcpy(flip, t + 5, 5 * sizeof(int));
    memcpy(flip + 5, t, 5 * sizeof(int));

    print_table(t);
    print_table(flip);
}