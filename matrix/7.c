#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void print_arr(int arr[5]) {
    for (int i = 0; i < 5; i++) {
        printf("%3d ", arr[i]);
    }
    printf("\n");
}

void print_matrix(int matrix[5][5]) {
    for(int i = 0; i < 5; i++) {
        print_arr(matrix[i]);
    }
}

void get_przeciwprzekatna(int matrix[5][5], int arr[5]) {
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if( j == 4-i ) {
                arr[i] = matrix[i][j];
            }
        }
    }
}

void set_przeciwprzekatna(int matrix[5][5], int arr[5]) {
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            if( j == 4-i ) {
                matrix[i][j] = arr[i];
            }
        }
    }
}

void rotate_array(int arr[5]) {
    int temp[5];
    for (int i = 0; i < 5; i++) {
        temp[4-i] = arr[i];
    }
    for (int i = 0; i < 5; i++) {
        arr[i] = temp[i];
    }
}

int main(void) {
    srand(time(NULL));

    int matrix[5][5];
    int arr[5];

    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            matrix[i][j] = rand() % 16;
        }
    }

    print_matrix(matrix);

    printf("========================================\n");

    get_przeciwprzekatna(matrix, arr);

    rotate_array(arr);

    set_przeciwprzekatna(matrix, arr);

    print_matrix(matrix);
}