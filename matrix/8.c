#include <stdio.h>
#include <string.h>

void print_arr(char arr[10][200]) {
    for (int i = 0; i < 10; i++) {
        printf("%s ", arr[i]);
    }
    printf("\n");
}

int get_longest(char arr[10][200]) {
    int max = 0;
    int max_index = -1;
    for (int i = 0; i < 10; i++) {
        if(strlen(arr[i]) > max) {
            max = strlen(arr[i]);
            max_index = i;
        }
    }
    return max_index;
}

int main(void) {
    char arr[10][200];

    printf("napisz 10 wyrazow, rozdzielaj znakiem nowej linii\n");

    for (int i = 0; i < 10; i++) {
        fgets(arr[i], 200, stdin);
        arr[i][strlen(arr[i]) - 1] = '\0';
    }

    print_arr(arr);

    printf("najdluzszy to:\n");
    printf("%s\n", arr[ get_longest(arr) ]);
}