#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void print_arr(double arr[4]) {
    for (int i = 0; i < 4; i++) {
        printf("%f ", arr[i]);
    }
    printf("\n");
}

void print_matrix(double matrix[3][4]) {
    for(int i = 0; i < 3; i++) {
        print_arr(matrix[i]);
    }
}

void multiply_matrix(double matrix[3][4], double skalar){
   for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            matrix[i][j] *= skalar;
        }
    }
}

int main(void) {
    srand(time(NULL));

    double matrix[3][4];
    int skalar;

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 4; j++) {
            matrix[i][j] = (rand() % 23) - 11;
        }
    }

    print_matrix(matrix);

    scanf("%d", &skalar);   

    multiply_matrix(matrix, skalar); 

    print_matrix(matrix);
}