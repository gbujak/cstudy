#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void print_arr(int arr[4]) {
    for (int i = 0; i < 4; i++) {
        printf("%3d ", arr[i]);
    }
    printf("\n");
}

void print_matrix(int matrix[4][4]) {
    for(int i = 0; i < 4; i++) {
        print_arr(matrix[i]);
    }
}

void sum_columns(int matrix[4][4], int colsum[]) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            colsum[j] += matrix[i][j];
        }
    }
}

void sum_wiersze(int matrix[4][4], int wierszsum[]) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            wierszsum[i] += matrix[i][j];
        }
    }
}

int main(void) {
    srand(time(NULL));

    int matrix[4][4];
    int skalar;
    int colsum[4] = {0};
    int wierszsum[4] = {0};

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            matrix[i][j] = (rand() % 41) - 20;
        }
    }

    print_matrix(matrix);

    sum_columns(matrix, colsum);
    sum_wiersze(matrix, wierszsum);

    printf("wiersze\n");
    print_arr(wierszsum);

    printf("kolumny\n");
    print_arr(colsum);
}
