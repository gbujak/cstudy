#include <stdio.h>

void count_chars(char input[256], int chars[256]) {
    for (int i = 0; input[i] != '\0' && i < 256; i++) {
        chars[ input[i] ]++;
    }
}

void print_chars(int chars[256], char start, char end) {
    for (int i = start; i < end; i++) {
        printf("%c%d ", i, chars[i]);
    }
    printf("\n");
}

int main(void) {
    char input[256];
    int chars[256] = {0}; //wszystkie wartości będą wynosić 0

    fgets(input, 256, stdin);

    count_chars(input, chars);

    // ASCII
    // a -> 97
    // d -> 100
    print_chars(chars, 97, 101);

    return 0;
}