#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void printarr(int arr[4]) {
    for (int i = 0; i < 3; i++) {
        printf("%3d, ", arr[i]);
    }
    printf("%d\n", arr[3]);
}

void printmatrix(int arr[4][4]) {
    printf("[\n");
    for (int i = 0; i < 4; i++) {
        printarr(arr[i]);
    }
    printf("]\n");
}

void arrtomatrix(int arr[], int matr[4][4]) {

    int n = 0;

    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            matr[i][j] = arr[n]; n++;
        }
    }

}

void swap(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

void sort(int arr[]) {
    int j, x;
    int i = 1;
    while(i < 16) {
        x = arr[i];
        j = i - 1;
        while(j >= 0 && arr[j] > x) {
            arr[j+1] = arr[j];
            j--;
        }
        arr[j+1] = x;
        i++;
    }
}

int main(void) {
    int matrix[4][4] = {{0}};
    int arr[16] = {0};
    srand(time(NULL));
    
    for (int i = 0; i < 16; i++) {
        arr[i] = (rand() % 21) - 10;
    }

    arrtomatrix(arr, matrix);

    sort(arr);

    arrtomatrix(arr, matrix);

    printmatrix(matrix);

}