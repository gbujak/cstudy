#include <stdio.h>
#include <stdlib.h>

#define LENGTH 5

int *reverse(int t[]);
void print_table(int t[]);

int main(void){
	int *table = malloc(LENGTH*sizeof(int));

	for(int i = 0; i < LENGTH; i++){
		scanf("%d", table + i);
	}

	print_table(table);

	int *rev_table = reverse(table);

	print_table(rev_table);

	free(table);
	free(rev_table);
}

int *reverse(int t[]){

	int *result = malloc(LENGTH*sizeof(int));

	for(int i = 0; i < LENGTH; i++){
		result[(LENGTH - 1) - i] = t[i];
	}

	return result;
}

void print_table(int t[]){
	for(int i = 0; i < LENGTH; i++) {
		printf("%d ", t[i]);
	}
	printf("\n");
}
