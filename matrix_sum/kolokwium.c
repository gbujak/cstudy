#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void randomize_matrix (int matrix[4][4]) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            matrix[i][j] = rand() % 101;
        }
    }
}

void print_array(int arr[4]) {
    for (int i = 0; i < 4; i++) {
        printf("%3d, ", arr[i]);
    }
    printf("\n");
}

void print_matrix(int matrix[4][4]) {
    for (int i = 0; i < 4; i++) {
        print_array(matrix[i]);
    }
}

int sum_matrix(int matrix[4][4]) {
    int sum = 0;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            sum += matrix[i][j];
        }
    }
    return sum;
}

int add_matrixes(int matrix1 [4][4],
                 int matrix2 [4][4],
                 int result  [4][4]) {
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            result[i][j] = matrix1[i][j] + matrix2[i][j];
        }
    }
}

int main (void) {
    int matrix1[4][4];
    int matrix2[4][4];
    int matrix_added[4][4];

    srand(time(NULL));

    randomize_matrix(matrix1);
    randomize_matrix(matrix2);

    puts("wylosowane macierze:");

    print_matrix(matrix1);
    puts("=============================");
    print_matrix(matrix2);

    puts("suma macierzy 1 z macierza 2:");
    add_matrixes(matrix1, matrix2, matrix_added);

    print_matrix(matrix_added);

    puts("suma wszystkich wartości macierzy 3 wynosi:");

    printf("%d\n", sum_matrix(matrix_added));
}