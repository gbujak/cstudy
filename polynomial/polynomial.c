#include <stdio.h>

#define DEG 2

float polynomial (float values[], float x);

int main (void) {
    float values[DEG + 1];
    for (int i = 0; i <= DEG; i++) {
        scanf("%f", values + i);
    }
    for (int i = DEG; i >= 1; i--) {
        printf("%f * x^%d + ", values[i], i);
    }
    printf("%f * x^%d", values[0], 0);
    float x;
    while (1) {
        printf(" \n\tx0 = ");
        scanf("%f", &x);
        printf("%f", polynomial(values, x));
    }
}

float polynomial (float values[], float x) {
    float bn = values[DEG];
    for (int i = DEG - 1; i >= 0; i--) {
        bn = values[i] + bn * x;
    }
    return bn;
}
