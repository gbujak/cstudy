#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

struct point {
    int x,y,special;
};

double point_dist(struct point a, struct point b) {
    int x = abs(a.x - b.x);
    int y = abs(a.y - b.y);

    return sqrt( pow(x, 2) + pow(y, 2) );
}

void print_point(struct point p) {
    printf("(%d, %d) ", p.x, p.y);
}

void print_arr(struct point p[]) {
    printf("[");
    for (int i = 0; i < 10; i++) {
        print_point(p[i]);
    }
    printf("]\n");
}

void mark_farthest(struct point arr[], int farthest[]) {
    double maxdist;
    double temp;

    for (int i = 0; i < 10; i++) {
        for (int j = 0; j < 10; j++) {
            if (i == j) {
                continue;
            }
            if ( (temp = point_dist(arr[i], arr[j])) > maxdist ) {
                maxdist = temp;
                farthest[0] = i;
                farthest[1] = j;
            }
        }
    }
}

int is_point(int x, int y, struct point arr[]) {
    for (int i = 0; i < 10; i++) {
        if(arr[i].x == x && arr[i].y == y) {
            if (arr[i].special == 1) {
                return 2;
            }
            return 1;
        }
    }
    return 0;
}

void print_map(struct point arr[]) {
    puts("+==========================================+");
    for (int i = -10; i <= 10; i++) {
        putc('|', stdout);
        for (int j = -10; j <= 10; j++) {
            int point = is_point(j, i, arr);
            switch (point) {
                case 1:
                    printf("x ", stdout);
                    break;
                case 2:
                    printf("@ ", stdout);
                    break;
                default:
                    printf("  ", stdout);
                    break;
            }
        }
        putc('|', stdout);
        putc('\n', stdout);
    }
    puts("+==========================================+");
}

int main (void) {

    srand(time(NULL));

    struct point arr[10];

    int farthest[2];

    for (int i = 0; i < 10; i++) {
        arr[i].x = (rand() % 21) - 10;
        arr[i].y = (rand() % 21) - 10;
        arr[i].special = 0;
    }

    mark_farthest(arr, farthest); 

    arr[farthest[0]].special = 1;
    arr[farthest[1]].special = 1;

    print_map(arr);

    puts("tablica punktów:");
    print_arr(arr);

    puts("Najbardziej oddalone punkty:");
    print_point(arr[farthest[0]]);
    print_point(arr[farthest[1]]);
    printf("\n");
}